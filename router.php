<?php
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $route) {
    $route->addRoute('GET', '/{name}', 'Controller\\Image@getImage');
});

include ('src/system/routerProcessor.php');