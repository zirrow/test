<?php

namespace Controller;

use Model\ImageModel;
use Helper\ResponseHelper;
use Model\UserModel;
use Service\UserDataService;

class Image
{
    /**
     * @param array $request
     * @param ImageModel $imageModel
     */
    public function getImage(
        array $request,
        ImageModel $imageModel,
        UserDataService $userDataService,
        UserModel $userModel)
    {
        $image = $imageModel->getImageData($request['name']);
        if (empty($image)) {
            return ResponseHelper::JsonResponse(['error' => 'Image not found'], 404);
        }

        $userData = $userDataService->getUserData($image);
        $userModel->processUserData($userData);

        return ResponseHelper::FileResponse($image);
    }
}