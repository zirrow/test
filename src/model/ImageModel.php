<?php

namespace Model;

class ImageModel extends Model
{
    /**
     * @param string $image
     * @return bool
     */
    public function checkImageIsset(string $image): bool
    {
        return $this->getColumn("SELECT COUNT(id) FROM images WHERE name = '".$image."'");
    }

    /**
     * @param string $image
     * @return mixed
     */
    public function getImageData(string $image)
    {
        return $this->getRow("SELECT * FROM images WHERE name = '".$image."'");
    }
}