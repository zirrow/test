<?php

namespace Model;

use http\Exception\BadQueryStringException;

class Model
{
    public $DB;

    public function __construct()
    {
        $dsn = "mysql:host=".DB_HOSTNAME.";dbname=".DB_USERNAME.";charset=".DB_CHARSET;
        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {
            $this->DB = new \PDO($dsn, DB_USERNAME, DB_PASSWORD, $opt);
        } catch (\PDOException $e) {
            die('Connection to database FAIL: ' . $e->getMessage());
        }
    }

    /**
     * @param string $query
     * @return array|false
     */
    public function getRows(string $query)
    {
        return $this->DB->query($query)->fetchAll();
    }

    /**
     * @param string $query
     * @return mixed
     */
    public function getRow(string $query)
    {
        $result = $this->getRows($query);

        return array_pop($result);
    }

    public function getColumn(string $query)
    {
        return $this->DB->query($query)->fetchColumn();
    }
}