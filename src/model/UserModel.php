<?php

namespace Model;

use Carbon\Carbon;

class UserModel extends Model
{
    /**
     * @param array $userData
     */
    public function processUserData(array $userData): void
    {
        $check = $this->getColumn(
            "SELECT COUNT(id) FROM user_activity WHERE fingerprint = '".$userData['finger_print']."'"
        );

        if ($check) {
            $this->incrementUserStat($userData);
        } else {
            $this->createUserStat($userData);
        }
    }

    /**
     * @param $userData
     */
    private function incrementUserStat($userData): void
    {
        $this->getColumn(
            "UPDATE user_activity
                        SET views_count = views_count + 1, view_date = '".Carbon::now()->format('Y-m-d H:i:s')."'
                        WHERE fingerprint = '".$userData['finger_print']."'"
        );
    }

    /**
     * @param $userData
     */
    private function createUserStat($userData): void
    {
        $this->getColumn(
            "INSERT INTO user_activity (image_id, ip_address, user_agent, view_date, page_url, views_count, fingerprint)
                    VALUES ('".$userData['image_id']."', 
                    '".$userData['ip']."', 
                    '".$userData['user_agent']."', 
                    '".Carbon::now()->format('Y-m-d H:i:s')."', 
                    '".$userData['page_url']."', 
                    '1',
                    '".$userData['finger_print']."')"
        );
    }
}