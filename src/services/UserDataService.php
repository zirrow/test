<?php

namespace Service;

class UserDataService
{
    /**
     * @return array
     */
    public function getUserData(array $image = []): array
    {
        $userData = [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'page_url' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['SERVER_NAME'],
        ];

        if (!empty($image)) {
            $userData['image_id'] = $image['id'];
        }

        $userData['finger_print'] = $this->createUserFingerprint($userData);

        return $userData;
    }

    /**
     * @param array $userData
     * @return string
     */
    private function createUserFingerprint(array $userData): string
    {
        $hashString = $userData['ip'].'|'.$userData['user_agent'].'|'.$userData['page_url'];

        if (isset($userData['image_id'])) {
            $hashString = $userData['image_id'].'|'.$userData['ip'].'|'.$userData['user_agent'].'|'.$userData['page_url'];
        }

        return md5($hashString);
    }
}