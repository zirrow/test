<?php
// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        Helper\ResponseHelper::JsonResponse(['error' => '404'], 404);
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        Helper\ResponseHelper::JsonResponse(['error' => '405'], 405);
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = explode('@', $routeInfo[1]);
        $vars = $routeInfo[2];

        $dependensies = (new \Helper\DependencyHelper())->getDependencies($handler[0]);

        (new $handler[0](...$dependensies))->{$handler[1]}($vars, ...$dependensies);

        break;
}