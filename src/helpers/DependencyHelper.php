<?php

namespace Helper;

class DependencyHelper
{
    /**
     * @var \string[][]
     */
    private $dependencies = [
        'Controller\\Image' => [
            'Model\\ImageModel',
            'Service\\UserDataService',
            'Model\\UserModel'
        ]
    ];

    /**
     * @param string $class
     * @return array
     */
    public function getDependencies(string $class): array
    {
        $objects = [];

        if (key_exists($class, $this->dependencies)) {
            foreach ($this->dependencies[$class] as $className) {
                $objects[] = new $className;
            }
        }

        return $objects;
    }
}