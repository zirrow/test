<?php

namespace Helper;

class ResponseHelper
{
    /**
     * @param array $message
     * @param int $status
     */
    public static function JsonResponse(array $message, int $status = 200)
    {
        header('Content-Type: application/json; charset=utf-8');

        header(self::getHeaderCode($status));

        echo json_encode($message);
    }

    /**
     * @param int $status
     * @return string
     */
    protected static function getHeaderCode(int $status): string
    {
        $response = "HTTP/1.1 200 OK";

        $codes = [
            200 => "HTTP/1.1 200 OK",
            400 => "HTTP/1.1 404 Bad Request",
            404 => "HTTP/1.1 404 Not Found",
            405 => "HTTP/1.1 404 Method Not Allowed",
            500 => "HTTP/1.1 500 Internal Server Error"
        ];

        if (key_exists($status, $codes)) {
            return $codes[$status];
        }

        return $response;
    }

    /**
     * @param array $file
     */
    public static function FileResponse(array $file)
    {
       $fullFileLink = IMAGE_DIR.$file['link'];

        if (!is_file($fullFileLink)) {
            self::JsonResponse(['error' => 'Image not found'], 404);
            die();
        }

        \header("Content-type: ".mime_content_type($fullFileLink));
        \header('Content-Length: '.\filesize($fullFileLink));
        \header('Last-Modified: '.\filemtime($fullFileLink));
        \header('Custom: statistics');

        \readfile($fullFileLink);
    }
}